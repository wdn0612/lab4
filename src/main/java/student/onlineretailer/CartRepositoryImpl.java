package student.onlineretailer;

import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

@Component
public class CartRepositoryImpl implements CartRepository {

    private HashMap<Integer, Integer> itemList = new HashMap<>();

    @Override
    public void add(int itemId, int quantity) {
        Integer existingQuantity = itemList.get(itemId);
        if (existingQuantity != null) {
            quantity += existingQuantity;
        }
        itemList.put(itemId,  quantity);
    }

    @Override
    public void remove(int itemId) {
        itemList.remove(itemId);
    }

    @Override
    public Map<Integer, Integer> getAll() {
        return itemList;
    }
}
