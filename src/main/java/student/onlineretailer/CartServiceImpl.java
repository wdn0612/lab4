package student.onlineretailer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;

@Component
public class CartServiceImpl implements CartService{

    @Value("${contactEmail}")
    public String contactEmail;

    @Value("${onlineRetailer.salesTaxRate}")
    public double salesTaxRate;

    @Autowired
    private CartRepository repository;

    @Value("#{catalog}")
    private Map<Integer, Item> catalog;

    @Override
    public void addItemToCart(int id, int quantity) {
        Iterator<Map.Entry<Integer, Item> > it = catalog.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            if (id==(int) pair.getKey()){
                repository.add(id,quantity);
            }
        }
    }

    @Override
    public void removeItemFromCart(int id) {
        repository.remove(id);
    }

    @Override
    public Map<Integer, Integer> getAllItemsInCart() {
        return repository.getAll();
    }

    @Override
    public double calculateCartCost() {
        double totalCost = 0;
        Map<Integer,Integer> allItems = getAllItemsInCart();
        Iterator it = allItems.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            totalCost += catalog.get(pair.getKey()).getPrice() * (int) pair.getValue();
        }
        return totalCost;
    }
}
