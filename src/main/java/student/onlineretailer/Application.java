package student.onlineretailer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Map;
import java.util.HashMap;

@SpringBootApplication
@EnableConfigurationProperties
public class Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);
		CartServiceImpl cartService = context.getBean(CartServiceImpl.class);

		System.out.println(cartService.contactEmail);
		System.out.println("Sales tax rate: "+cartService.salesTaxRate);
		System.out.println("Initial cart: "+cartService.getAllItemsInCart());
		cartService.addItemToCart(2,2);
		cartService.addItemToCart(0,1);
		System.out.println("My cart: "+cartService.getAllItemsInCart());
		System.out.println("Total cost before removing: "+cartService.calculateCartCost());
		cartService.removeItemFromCart(2);
		System.out.println("Total cost after removing: "+cartService.calculateCartCost());

		// Get profile-specific properties.
		ResourcesBean resourcesBean = context.getBean(ResourcesBean.class);
		System.out.println("Profile-specific properties: " + resourcesBean);
	}

	@Bean
	public Map<Integer, Item> catalog() {
		Map<Integer, Item> items = new HashMap<>();
		items.put(0, new Item(0, "Apple Mac Book Pro", 2499.99));
		items.put(1, new Item(1, "32GB San Disk", 15.99));
		items.put(2, new Item(2, "OLED 64in TV", 1800));
		items.put(3, new Item(3, "Wireless Mouse", 10.50));
		items.put(4, new Item(4, "Virtual Reality HeadSet", 200));
		return items;
	}
}
